# SSL-Stripping with Vagrant and Bettercap

This repository contains all necessary files to spin up a lab to play around with MITM attacks like SSL stripping.
For a detailed step-by-step guide, visit https://medium.com/@Felix_51570/ssl-stripping-demonstration-and-mitigation-5fe05ed924d8
